import ChatAppDispatcher from '../dispatcher';
import ChatConstants from '../constants';
import ChatWebAPIUtils from '../data/utils/ChatWebAPIUtils';
import ChatMessageUtils from '../data/utils/ChatMessageUtils';

const ActionTypes = ChatConstants.ActionTypes;

export default {
    createMessage(text, currentThreadID) {
        ChatAppDispatcher.dispatch({
            type: ActionTypes.CREATE_MESSAGE,
            text,
            currentThreadID,
        });
        const message = ChatMessageUtils.getCreatedMessageData(text, currentThreadID);
        ChatWebAPIUtils.createMessage(message);
    },
};
