import ChatAppDispatcher from '../dispatcher';
import ChatConstants from '../constants';

const ActionTypes = ChatConstants.ActionTypes;

export default {
    clickThread(threadID) {
        ChatAppDispatcher.dispatch({
            type: ActionTypes.CLICK_THREAD,
            threadID,
        });
    },
};
