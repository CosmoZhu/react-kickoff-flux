import ChatAppDispatcher from '../dispatcher';
import ChatConstants from '../constants';

const ActionTypes = ChatConstants.ActionTypes;

export default {
    receiveAll(rawMessages) {
        ChatAppDispatcher.dispatch({
            type: ActionTypes.RECEIVE_RAW_MESSAGES,
            rawMessages,
        });
    },

    receiveCreatedMessage(createdMessage) {
        ChatAppDispatcher.dispatch({
            type: ActionTypes.RECEIVE_RAW_CREATED_MESSAGE,
            rawMessage: createdMessage,
        });
    },
};
