import React, { PropTypes } from 'react';
import ChatMessageActionCreators from '../../actions/ChatMessageActionCreators';

const ENTER_KEY_CODE = 13;

const MessageComposer = React.createClass({
    propTypes: {
        threadID: PropTypes.string.isRequired,
    },

    getInitialState() {
        return { text: '' };
    },

    onChange(event) {
        this.setState({ text: event.target.value });
    },

    onKeyDown(event) {
        if (event.keyCode === ENTER_KEY_CODE) {
            event.preventDefault();
            const text = this.state.text.trim();
            if (text) {
                ChatMessageActionCreators.createMessage(text, this.props.threadID);
            }
            this.setState({ text: '' });
        }
    },

    render() {
        return (
            <textarea
                className="message-composer"
                name="message"
                value={this.state.text}
                onChange={this.onChange}
                onKeyDown={this.onKeyDown}
            />
        );
    },
});

export default MessageComposer;
