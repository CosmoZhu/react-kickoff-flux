import React, { PropTypes } from 'react';

function MessageListItem(props) {
    return (
        <li className="message-list-item">
            <h5 className="message-author-name">{props.message.authorName}</h5>
            <div className="message-time">
                {props.message.date.toLocaleTimeString() }
            </div>
            <div className="message-text">{props.message.text}</div>
        </li>
    );
}

MessageListItem.propTypes = {
    message: PropTypes.object,
};

export default MessageListItem;
