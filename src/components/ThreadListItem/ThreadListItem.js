import React, { PropTypes } from 'react';
import classNames from 'classnames';
import ChatThreadActionCreators from '../../actions/ChatThreadActionCreators';

const ThreadListItem = React.createClass({
    propTypes: {
        thread: PropTypes.object,
        currentThreadID: PropTypes.string,
    },

    onClick() {
        ChatThreadActionCreators.clickThread(this.props.thread.id);
    },

    render() {
        const thread = this.props.thread;
        const lastMessage = thread.lastMessage;

        return (
            /* eslint-disable jsx-a11y/no-static-element-interactions */
            <li
                className={classNames({
                    'thread-list-item': true,
                    'active': thread.id === this.props.currentThreadID,
                })}
                onClick={this.onClick}
            >
                <h5 className="thread-name">{thread.name}</h5>
                <div className="thread-time">
                    {lastMessage.date.toLocaleTimeString() }
                </div>
                <div className="thread-last-message">
                    {lastMessage.text}
                </div>
            </li>
            /* eslint-enable jsx-a11y/no-static-element-interactions */
        );
    },
});

export default ThreadListItem;
