import React from 'react';
import MessageComposer from '../MessageComposer';
import MessageListItem from '../MessageListItem';
import MessageStore from '../../stores/MessageStore';
import ThreadStore from '../../stores/ThreadStore';

function getStateFromStores() {
    return {
        messages: MessageStore.getAllForCurrentThread(),
        thread: ThreadStore.getCurrent(),
    };
}

function getMessageListItem(message) {
    return (
        <MessageListItem
            key={message.id}
            message={message}
        />
    );
}

const MessageSection = React.createClass({
    getInitialState() {
        return getStateFromStores();
    },

    componentDidMount() {
        this.scrollToBottom();
        MessageStore.addChangeListener(this.onChange);
        ThreadStore.addChangeListener(this.onChange);
    },

    componentDidUpdate() {
        this.scrollToBottom();
    },

    componentWillUnmount() {
        MessageStore.removeChangeListener(this.onChange);
        ThreadStore.removeChangeListener(this.onChange);
    },

    /**
     * Event handler for 'change' events coming from the MessageStore
     */
    onChange() {
        this.setState(getStateFromStores());
    },

    scrollToBottom() {
        const ul = this.messageList;
        ul.scrollTop = ul.scrollHeight;
    },

    render() {
        const messageListItems = this.state.messages.map(getMessageListItem);
        return (
            <div className="message-section">
                <h3 className="message-thread-heading">{this.state.thread.name}</h3>
                <ul className="message-list" ref={(ref) => (this.messageList = ref)}>
                    {messageListItems}
                </ul>
                <MessageComposer threadID={this.state.thread.id} />
            </div>
        );
    },
});

export default MessageSection;
