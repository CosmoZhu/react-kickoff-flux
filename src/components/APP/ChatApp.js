import React from 'react';
import './ChatApp.less';
import ThreadSection from '../ThreadSection';
import MessageSection from '../MessageSection';

function ChatApp() {
    return (
        <div className="chatapp">
            <ThreadSection />
            <MessageSection />
        </div>
    );
}

export default ChatApp;
