import React from 'react';
import ThreadListItem from '../ThreadListItem';
import ThreadStore from '../../stores/ThreadStore';
import UnreadThreadStore from '../../stores/UnreadThreadStore';

function getStateFromStores() {
    return {
        threads: ThreadStore.getAllChrono(),
        currentThreadID: ThreadStore.getCurrentID(),
        unreadCount: UnreadThreadStore.getCount(),
    };
}

const ThreadSection = React.createClass({
    getInitialState() {
        return getStateFromStores();
    },

    componentDidMount() {
        ThreadStore.addChangeListener(this.onChange);
        UnreadThreadStore.addChangeListener(this.onChange);
    },

    componentWillUnmount() {
        ThreadStore.removeChangeListener(this.onChange);
        UnreadThreadStore.removeChangeListener(this.onChange);
    },

    /**
     * Event handler for 'change' events coming from the stores
     */
    onChange() {
        this.setState(getStateFromStores());
    },

    render() {
        const threadListItems = this.state.threads.map(function (thread) {
            return (
                <ThreadListItem
                    key={thread.id}
                    thread={thread}
                    currentThreadID={this.state.currentThreadID}
                />
            );
        }, this);
        const unread = this.state.unreadCount === 0
            ? null
            : <span>Unread threads: {this.state.unreadCount}</span>;
        return (
            <div className="thread-section">
                <div className="thread-count">
                    {unread}
                </div>
                <ul className="thread-list">
                    {threadListItems}
                </ul>
            </div>
        );
    },
});

module.exports = ThreadSection;
