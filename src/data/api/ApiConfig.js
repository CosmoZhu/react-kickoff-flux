/**
 * API config for PC/Client
 */

const apiBaseUrl = '/api/'; // change yours

const urlMap = {
    'someApi': `${apiBaseUrl}some/path/to`,
    'otherApi': `${apiBaseUrl}some/path/to`,
};

export default urlMap;
