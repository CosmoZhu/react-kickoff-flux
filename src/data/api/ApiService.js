/**
 * @description
 * Using XMLHttpRequest api to achieve asynchronous requests
 *
 * @example
   import { oneApi, anotherApi } from 'path/to/core/superagent';

   oneApi({
        foo: someValue,
        bar: someValue,
    }).then((responseData) => {
        this.result = responseData.data;
    });

    anotherApi({
        foo: someValue,
        bar: someValue,
    }).then((responseData) => {
        if (responseData.success) {
            // some success logic
        } else {
            // some exception logic
        }
    });
 */

import http from '../../core/superagent';
import urlMap from './ApiConfig';

export const oneApi = async (requestConfig) => {
    const url = urlMap.someApi;
    const data = await http.get(url, requestConfig);
    return data;
};

export const anotherApi = async (requestConfig) => {
    const url = urlMap.otherApi;
    const data = await http.post(url, requestConfig);
    return data;
};
