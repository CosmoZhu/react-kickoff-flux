/**
 * @description
 * Using fetch api to achieve asynchronous requests
 *
 * @example
   import { oneApi, anotherApi } from 'path/to/core/fetch';

   oneApi({
        foo: someValue,
        bar: someValue,
    }).then((responseData) => {
        this.result = responseData.data;
    });

    anotherApi({
        foo: someValue,
        bar: someValue,
    }).then((responseData) => {
        if (responseData.success) {
            // some success logic
        } else {
            // some exception logic
        }
    });
 */

import fetch from '../../core/fetch';
import urlMap from './ApiConfig';

export const oneApi = async (requestConfig) => {
    const url = urlMap.someApi;
    const response = await fetch(`${url}?param1=${requestConfig.foo}&param2=${requestConfig.bar}`);
    const data = await response.json();
    return data;
};

export const anotherApi = async (requestConfig) => {
    const url = urlMap.otherApi;
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestConfig),
    });
    const data = await response.json();
    return data;
};
