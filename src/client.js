import 'babel-polyfill';
import React from 'react'; // eslint-disable-line no-unused-vars
import ReactDOM from 'react-dom';
import FastClick from 'fastclick';
import ChatApp from './components/App';
import ChatExampleData from './data/ChatExampleData';
import ChatWebAPIUtils from './data/utils/ChatWebAPIUtils';

function bootstrap() {
    // Make taps on links and buttons work fast on mobiles
    FastClick.attach(document.body);

    // load example data into localstorage
    ChatExampleData.init();

    ChatWebAPIUtils.getAllMessages();

    ReactDOM.render(
        <ChatApp />,
        document.getElementById('reactapp')
    );
}

// Run the application when both DOM is ready and page content is loaded
if (['complete', 'loaded', 'interactive'].includes(document.readyState) && document.body) {
    bootstrap();
} else {
    document.addEventListener('DOMContentLoaded', bootstrap, false);
}
