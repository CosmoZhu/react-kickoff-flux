import EventEmitter from 'eventemitter3';
import ChatAppDispatcher from '../dispatcher';
import ChatConstants from '../constants';
import MessageStore from '../stores/MessageStore';
import ThreadStore from '../stores/ThreadStore';

const ActionTypes = ChatConstants.ActionTypes;
const CHANGE_EVENT = 'change';

const UnreadThreadStore = Object.assign({}, EventEmitter.prototype, {
    emitChange() {
        this.emit(CHANGE_EVENT);
    },

    /**
     * @param {function} callback
     */
    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    /**
     * @param {function} callback
     */
    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    getCount() {
        const threads = ThreadStore.getAll();
        let unreadCount = 0;
        for (const id in threads) {
            if (!threads[id].lastMessage.isRead) {
                unreadCount++;
            }
        }
        return unreadCount;
    },
});

UnreadThreadStore.dispatchToken = ChatAppDispatcher.register((action) => {
    ChatAppDispatcher.waitFor([
        ThreadStore.dispatchToken,
        MessageStore.dispatchToken,
    ]);

    switch (action.type) {
        case ActionTypes.CLICK_THREAD:
            UnreadThreadStore.emitChange();
            break;

        case ActionTypes.RECEIVE_RAW_MESSAGES:
            UnreadThreadStore.emitChange();
            break;

        default:
        // do nothing
    }
});

export default UnreadThreadStore;
