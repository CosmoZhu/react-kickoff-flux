import EventEmitter from 'eventemitter3';
import ChatAppDispatcher from '../dispatcher';
import ChatConstants from '../constants';
import ChatMessageUtils from '../data/utils/ChatMessageUtils';
import ThreadStore from '../stores/ThreadStore';

const ActionTypes = ChatConstants.ActionTypes;
const CHANGE_EVENT = 'change';

const messages = {};
let message;

function addMessages(rawMessages) {
    rawMessages.forEach((messageItem) => {
        if (!messages[messageItem.id]) {
            messages[messageItem.id] = ChatMessageUtils.convertRawMessage(
                messageItem,
                ThreadStore.getCurrentID()
            );
        }
    });
}

function markAllInThreadRead(threadID) {
    for (const id in messages) {
        if (messages[id].threadID === threadID) {
            messages[id].isRead = true;
        }
    }
}

const MessageStore = Object.assign({}, EventEmitter.prototype, {
    emitChange() {
        this.emit(CHANGE_EVENT);
    },

    /**
     * @param {function} callback
     */
    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    /**
     * @param {function} callback
     */
    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    get(id) {
        return messages[id];
    },

    getAll() {
        return messages;
    },

    /**
     * @param {string} threadID
     */
    getAllForThread(threadID) {
        const threadMessages = [];
        for (const id in messages) {
            if (messages[id].threadID === threadID) {
                threadMessages.push(messages[id]);
            }
        }
        threadMessages.sort((a, b) => {
            if (a.date < b.date) {
                return -1;
            } else if (a.date > b.date) {
                return 1;
            }
            return 0;
        });
        return threadMessages;
    },

    getAllForCurrentThread() {
        return this.getAllForThread(ThreadStore.getCurrentID());
    },
});

MessageStore.dispatchToken = ChatAppDispatcher.register((action) => {
    switch (action.type) {
        case ActionTypes.CLICK_THREAD:
            ChatAppDispatcher.waitFor([ThreadStore.dispatchToken]);
            markAllInThreadRead(ThreadStore.getCurrentID());
            MessageStore.emitChange();
            break;

        case ActionTypes.CREATE_MESSAGE:
            message = ChatMessageUtils.getCreatedMessageData(
                action.text,
                action.currentThreadID
            );
            messages[message.id] = message;
            MessageStore.emitChange();
            break;

        case ActionTypes.RECEIVE_RAW_MESSAGES:
            addMessages(action.rawMessages);
            ChatAppDispatcher.waitFor([ThreadStore.dispatchToken]);
            markAllInThreadRead(ThreadStore.getCurrentID());
            MessageStore.emitChange();
            break;

        default:
        // do nothing
    }
});

export default MessageStore;
