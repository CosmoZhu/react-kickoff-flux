import EventEmitter from 'eventemitter3';
import ChatAppDispatcher from '../dispatcher';
import ChatConstants from '../constants';
import ChatMessageUtils from '../data/utils/ChatMessageUtils';

const ActionTypes = ChatConstants.ActionTypes;
const CHANGE_EVENT = 'change';

let currentID = null;
const threads = {};

const ThreadStore = Object.assign({}, EventEmitter.prototype, {
    init(rawMessages) {
        rawMessages.forEach((message) => {
            const threadID = message.threadID;
            const thread = threads[threadID];
            if (thread && thread.lastMessage.timestamp > message.timestamp) {
                return;
            }
            threads[threadID] = {
                id: threadID,
                name: message.threadName,
                lastMessage: ChatMessageUtils.convertRawMessage(message, currentID),
            };
        }, this);

        if (!currentID) {
            const allChrono = this.getAllChrono();
            currentID = allChrono[allChrono.length - 1].id;
        }

        threads[currentID].lastMessage.isRead = true;
    },

    emitChange() {
        this.emit(CHANGE_EVENT);
    },

    /**
     * @param {function} callback
     */
    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    /**
     * @param {function} callback
     */
    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    /**
     * @param {string} id
     */
    get(id) {
        return threads[id];
    },

    getAll() {
        return threads;
    },

    getAllChrono() {
        const orderedThreads = [];
        for (const id in threads) {
            if ({}.hasOwnProperty.call(threads, id)) {
                const thread = threads[id];
                orderedThreads.push(thread);
            }
        }
        orderedThreads.sort((a, b) => {
            if (a.lastMessage.date < b.lastMessage.date) {
                return -1;
            } else if (a.lastMessage.date > b.lastMessage.date) {
                return 1;
            }
            return 0;
        });
        return orderedThreads;
    },

    getCurrentID() {
        return currentID;
    },

    getCurrent() {
        return this.get(this.getCurrentID());
    },
});

ThreadStore.dispatchToken = ChatAppDispatcher.register((action) => {
    switch (action.type) {
        case ActionTypes.CLICK_THREAD:
            currentID = action.threadID;
            threads[currentID].lastMessage.isRead = true;
            ThreadStore.emitChange();
            break;

        case ActionTypes.RECEIVE_RAW_MESSAGES:
            ThreadStore.init(action.rawMessages);
            ThreadStore.emitChange();
            break;

        default:
        // do nothing
    }
});

export default ThreadStore;
